// Make validate metod
function validateFunction() {

	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var eMail = document.getElementById("eMail").value;
	var regPassword = document.getElementById("regPassword").value;
	var confRegPassword = document.getElementById("confRegPassword").value;

	if (firstName != "" && lastName != "" && eMail != "" && regPassword != "") {

		if (firstName.match(/^[A-Z]/)) {

			if (firstName.match(/^[A-Z]+[A-Za-z]+$/)) {

				if (firstName.match(/^[A-Z]+[a-z]+$/)) {

					if (lastName.match(/^[A-Z]/)) {

						if (lastName.match(/^[A-Z]+[A-Za-z]+$/)) {

							if (lastName.match(/^[A-Z]+[a-z]+$/)) {

								if (eMail.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {

									if (regPassword.length >= 8) {

										if (regPassword.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]/)) {

											if (confRegPassword != "") {

												if (confRegPassword === regPassword) {

													var inputElement = document.getElementsByClassName("inputElement");
													for(var i=0; i<inputElement.length; i++){
														inputElement[i].value = "";
													}

													$('#registration').modal('hide');
													$('#message').modal('show');

												} else {
													showRedAlert("Sifre se ne poklapaju!");
													return false;
												}

											} else {
												showRedAlert("Porate potvrditi sifru!");
												return false;
											}

										} else {
											showRedAlert("Sifra mora sadrzati jedno veliko/malo slovo, jedan broj i specijalni karakter!");
											return false;
										}

									} else {
										showRedAlert("Password mora imati minimum 8 karaktera!");
										return false;
									}

								} else {
									showRedAlert("E-mail adresa nije validna!");
									return false;
								}

							} else {
								showRedAlert("Prezime ne sme sadrzati veliko slovo sem na pocetku!");
								return false;
							}

						} else {
							showRedAlert("Prezime ne sme sadrzati brojeve!");
							return false;
						}

					} else {
						showRedAlert("Prezime mora pocinjati velikim slovom!");
						return false;
					}

				} else {
					showRedAlert("Ime ne sme sadrzati veliko slovo sem na pocetku!");
					return false;
				}

			} else {
				showRedAlert("Ime ne sme sadrzati brojeve!");
				return false;
			}

		} else {
			showRedAlert("Ime mora pocinjati velikim slovom!");
			return false;
		}

	} else {
		showRedAlert("Molimo vas popunite sva polja!");
		return false;
	}
}

function alertText(text){
	var node = document.createTextNode(text);
	var element = document.getElementById('RedAlert');
	element.appendChild(node);
}

function showRedAlert(text) {
	var element = document.getElementById("RedAlert");

	for(var i=0; i<element.childNodes.length; i++){
		var curNode = element.childNodes[i];
		curNode.nodeValue = "";
	}
	$('#RedAlert').animate({opacity: 1}, 300);
	alertText(text);
}
// function hideAlerts() {
// 	var element = document.getElementById("RedAlert");
// 	for(var i=0; i<element.childNodes.length; i++){
// 		var curNode = element.childNodes[i];
// 		curNode.nodeValue = "";
// 	}
// 	element.style.visibility = "hidden";	
// }

var regButton = document.getElementById("regButton");
regButton.addEventListener('click', validateFunction);

var inputElement = document.getElementsByClassName("inputElement");
for(var i=0; i<inputElement.length;i++){
	inputElement[i].addEventListener('focus', function(){
		// var element = document.getElementById("RedAlert");
		// element.style.visibility = "hidden";
		$('#RedAlert').animate({opacity: 0}, 300);
	});
}